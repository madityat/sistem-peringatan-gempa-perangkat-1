const cron = require('node-cron');
const device = require('./device');
const rp = require('request-promise');

async function start () {
  const schedule = process.env.RUN_CRONJOB || '*/5 * * * * *';
  cron.schedule(schedule, async () => {
    const getStatus = await device.showStatus('cronjob', null);
    const hostUrl = (process.env.HOST_URL) ? `http://ta-gempa.apps.playcourt.id` : `http://localhost:8080`;
    const requestTime = new Date().toISOString();
    var options = {
      method: 'PUT',
      uri: hostUrl+`/device/sinyal?requestTime=${requestTime}`,
      body: [{
        node_name: `server-${getStatus.serverName}`,
        earthquake_strength: getStatus.avgKekuatan
      }],
      json: true
    };
    rp(options)
      .then(function (parsedBody) {
        console.log(requestTime+' : success');
      })
      .catch(function (err) {
        console.log(requestTime+' : '+err);
      });  
  });
}

module.exports = {
  start
}