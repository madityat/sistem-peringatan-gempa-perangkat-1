const rp = require('request-promise');

async function sendSMS() {
  var options = {
    method: 'POST',
    uri: 'https://rest.nexmo.com/sms/json',
    qs: {
      api_key: '2de9f7ea',
      api_secret: 'tgcFsVgCiCi4FSbQ'
    },
    form: {
      from: 'GEMPA',
      to: '6281395217204',
      text: global.pesan 
    },
    headers: {
      'User-Agent': 'Request-Promise',
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    json: true // Automatically parses the JSON string in the response
  };
  if (process.env.SMS) {
    return await rp(options);
  } else {
    return 'Notif blocked, not in production';
  }
}
async function sendEMAIL() {
}
module.exports = {
  sendSMS,
  sendEMAIL
}